connect-four
============

This is a connect four game coded as part of my programming portfolio. It is not intended to be a public project. If you like it and want to use it, great - but pull requests will be ignored as this repo&#39;s contents are intended to be 100% mine.

It is being developed on Ubuntu 12.04, using Python 2.7.3. Indentation style is that of emacs23 default.