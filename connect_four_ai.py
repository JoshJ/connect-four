"""
    A connect-four clone written for my programming portfolio.
    Copyright (C) 2012 Joshua Justice <joshj777@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import random
import copy
from connect_four_board import *

# This class will use polymorphism

class ConnectFourAI:
    def __init__(self):
        pass

    def pick_move(self, board):
        return self.pick_random_move(board)

    def pick_random_move(self, board):
        # Will return a random valid move
        result = random.randint(0, board.width - 1)
        while board.verify_move(result) is False:
            result = random.randint(0, board.width - 1)
        return result

    def find_win(self, board, color):
        """Used by multiple child classes
        Returns the first column at which the color in question can win.
        If there are multiples, it's not really relevant:
        The AI can claim a win with either of them,
        and the AI will lose if the human picks the other one.
        """
        for i in range(0, board.width):
            newboard = copy.deepcopy(board)
            if newboard.verify_move(i):
                newboard.make_move(color, i)
                if newboard.check_for_victory():
                    return i
        return False


class VeryEasyAI(ConnectFourAI):
    # Will select all moves randomly.
    # This is the inherited behavior.
    pass

class EasyAI(ConnectFourAI):
    # Will take wins and block wins.
    def pick_move(self, board):
        win = self.find_win(board, Node.Black)
        if win is not False:
            # AI can claim a win
            return win
        else:
            block = self.find_win(board, Node.Red)
            if block is not False:
                #AI can block a human win
                return block
            else:
                return self.pick_random_move(board)
            

class NormalAI(ConnectFourAI):
    # Will take wins and block wins, and avoid setting up wins if possible."
    def pick_move(self, board):
        win = self.find_win(board, Node.Black)
        if win is not False:
            # AI can claim a win
            return win
        else:
            block = self.find_win(board, Node.Red)
            if block is not False:
                #AI can block a human win
                return block
            else:
                return self.avoid_giving_wins(board)

    def avoid_giving_wins(self, board):
        """
        Returns the first column at which the color in question can win.
        If there are multiples, it's not really relevant:
        The AI can claim a win with either of them,
        and the AI will lose if the human picks the other one.
        """
        valid = range(0, board.width)
        for i in range(0, board.width):
            if board.verify_move(i) is False:
                valid.remove(i)
                break
            blackboard = copy.deepcopy(board)
            blackboard.make_move(Node.Black, i)
            # If "i" sets up a human win, then remove it from the range
            for j in range(0, board.width):
                redboard = copy.deepcopy(blackboard)
                redboard.make_move(Node.Red, j)
                if redboard.check_for_victory():
                    valid.remove(i)
                    break
        # Select randomly from the valid range
        output = random.sample(valid, 1)
        if output:
            return output[0]
        # If the range is fully empty, pick any legal move - every move loses.
        else:
            return self.pick_random_move(board)
