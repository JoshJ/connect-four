"""
    A connect-four clone written for my programming portfolio.
    Copyright (C) 2012 Joshua Justice <joshj777@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Node:
    # A node represents a single cell in the grid
    Empty, Red, Black = range(3)
    def __init__(self, color=Empty):
        # All cells start empty.
        self.color=color

class Column:
    # A column is a column of cells in the grid
    def __init__(self, height):
        self.height = height
        self.fill = height-1 # Location of the highest piece in this column.
        self.nodes = []
        for i in range(self.height):
            self.nodes.append(Node())
    
    def add_piece(self, color):
        """
        The piece is dropped from the top.
        This means it must be added to the lowest empty cell in the column.
        This is the highest number since 0,0 is the top left.
        """
        self.nodes[self.fill] = Node(color)
        self.fill = self.fill - 1 
        
    

class Board:
    def __init__(self, width=7, height=6):
        self.width = width
        self.height = height        
        self.create_board()

    def create_board(self):
        self.columns=[]
        for x in range(0, self.width):
            # Create X columns
            self.columns.append(Column(self.height))

    def draw_board(self):
        output = ""
        for y in range(0, self.height):
            output = output + "|"
            for column in self.columns:
                if column.nodes[y].color==Node.Empty:
                    output = output + "-|"
                elif column.nodes[y].color==Node.Red:
                    output = output + "X|"
                elif column.nodes[y].color==Node.Black:
                    output = output + "O|"
            output = output + "\n" # Move to the next line
        # Make the bottom of the board more clear
        output = output + "="*(self.width*2+1)
        output = output + "\n" # Move to the next line
        for i in range(0, self.width):
            output = output + " " + str(i)
        return output

    def make_move(self, turn, col):
        if self.verify_move(col):
            self.columns[col].add_piece(turn)
            return True
        else:
            return False        

    def verify_move(self, col):
        if self.columns[col].fill<0:
            return False
        else:
            return True

    def check_for_tie(self):
        for column in self.columns:
            if column.fill>=0:
                return False
        return Node.Empty

    def check_for_victory(self):
        vertical = self.check_for_vertical_win()
        horizontal = self.check_for_horizontal_win()
        diagonal_d = self.check_for_diagonal_win_downwards()
        diagonal_u = self.check_for_diagonal_win_upwards()
        
        if vertical is not False:
            return vertical
        elif horizontal is not False:
            return horizontal
        elif diagonal_d is not False:
            return diagonal_d
        elif diagonal_u is not False:
            return diagonal_u
        else:
            return self.check_for_tie()

    def check_for_vertical_win(self):
        for column in self.columns:
            counter = 0
            color = Node.Empty
            for node in column.nodes:
                if node.color != color:
                    # We've changed colors, reset the counter
                    color = node.color
                    counter = 1
                elif node.color != Node.Empty:
                    # Same color and it's not empty, so increment
                    counter = counter + 1
                    if counter >= 4:
                        return color
        return False

    def check_for_horizontal_win(self):
        for y in range(0, self.height):
            counter = 0
            color = Node.Empty
            for column in self.columns:
                node = column.nodes[y]
                if node.color != color:
                    # We've changed colors, reset the counter
                    color = node.color
                    counter = 1
                elif node.color != Node.Empty:
                    # Same color and it's not empty, so increment
                    counter = counter + 1
                    if counter >= 4:
                        return color
        return False


    def check_for_diagonal_win_downwards(self):
        """
        The diagonal code here is suboptimal.
        For one thing, there's a lot of redundancy.
        For instance, if (1,1) through (5,5) is a diagonal,
        3/4 of it will be checked when I check (0,0) forwards.
        However, this is easy to debug and it runs quickly enough.
        """
        for start_x in range(0, self.width-3):
            for start_y in range(0, self.height-3):
                group = []
                counter = 0
                color = Node.Empty
                for val in range(0,4):
                    x = start_x + val
                    y = start_y + val
                    node = self.columns[x].nodes[y]
                    if node.color != color:
                        # We've changed colors, reset the counter
                        color = node.color
                        counter = 1
                    elif node.color != Node.Empty:
                        # Same color and it's not empty, so increment
                        counter = counter + 1
                        if counter >= 4:
                            return color
        return False

    def check_for_diagonal_win_upwards(self):
        for start_x in range(0, self.width-3):
            for start_y in range(3, self.height):
                group = []
                counter = 0
                color = Node.Empty
                for val in range(0,4):
                    x = start_x + val
                    y = start_y - val
                    node = self.columns[x].nodes[y]
                    if node.color != color:
                        # We've changed colors, reset the counter
                        color = node.color
                        counter = 1
                    elif node.color != Node.Empty:
                        # Same color and it's not empty, so increment
                        counter = counter + 1
                        if counter >= 4:
                            return color
        return False
