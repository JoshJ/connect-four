"""
    A connect-four clone written for my programming portfolio.
    Copyright (C) 2012 Joshua Justice <joshj777@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import random
import sys

from connect_four_ai import *
from connect_four_board import *

print "Welcome to connect-four by Joshua Justice!"

# Select who has what colors
print "You will play red and the AI will play black."
print "(Red is X and Black is O.)"

# Select AI difficulty
print "Select what difficulty AI you want to play against."
print "0: Very Easy AI, 1: Easy AI, 2: Normal AI"
difficulty = raw_input("Your selection? ")
try:
    diff = int(difficulty)
except ValueError:
    sys.exit("You made an invalid input! Now exiting...")

if diff == 0:
    game_ai = VeryEasyAI()
elif diff == 1:
    game_ai = EasyAI()
elif diff == 2:
    game_ai = NormalAI()
else: 
    sys.exit("You made an invalid input! Now exiting...")

print "Proceeding at difficulty: " + difficulty

# Randomly select who goes first
turn = random.randint(Node.Red, Node.Black)

if turn == Node.Red:
    print "You will go first."
else:
    print "The computer will go first."

# Generate a board
board = Board(7,6)

# Display initial board state and help
print "Columns are numbered from left to right starting at 0."
print "When it is your turn, pick the number of the column you wish to play in."
print board.draw_board()


""" END OF INITIAL SETUP """

""" BEGIN MAIN GAME LOOP """

finished=False
while not finished:
    # Check turn indicator
    legal_move = False
    while not legal_move:
        # If it is a human's turn:
        if turn == Node.Red:
            input_valid=False
            while not input_valid:
                # Read from input
                c = raw_input("Which column do you want to drop in? ")
                # Verify move
                try:
                    col = int(c)
                    if col >= 0 and col < board.width:
                        input_valid=True
                    else:
                        print "That is not one of the valid column numbers."
                except ValueError:
                    print "That is not a valid input. Please enter a number."
        # If it is an AI's turn:
        if turn == Node.Black:
            # Generate move
            col = game_ai.pick_move(board)
            print "The AI drops a piece into column " + str(col)
        # Make that move
        legal_move=board.make_move(turn, col)
        if not legal_move:
            print "The move was not valid."

    # Check for victory
    result = board.check_for_victory()
    if result is not False:
        finished = True
    # Toggle turn status
    if turn == Node.Red:
        turn = Node.Black
    else:
        turn = Node.Red
    print board.draw_board()
if result == Node.Red:
    print "A winner is you!"
elif result == Node.Black:
    print "You lost..."
else:
    print "Cat's game."
