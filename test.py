"""
    A connect-four clone written for my programming portfolio.
    Copyright (C) 2012 Joshua Justice <joshj777@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import unittest

from connect_four_ai import *
from connect_four_board import *

class TestBoard(unittest.TestCase):
    def setUp(self):
        # Call before every test
        self.board = Board()

    def tearDown(self):
        # Call after every test
        pass # nothing at the moment

    def testMakeNode(self):
        n = Node()
        assert n.color==Node.Empty, "Created nodes are empty."

    def testDrawEmptyBoard(self):
        str = "|-|-|-|-|-|-|-|\n|-|-|-|-|-|-|-|\n|-|-|-|-|-|-|-|\n|-|-|-|-|-|-|-|\n|-|-|-|-|-|-|-|\n|-|-|-|-|-|-|-|\n===============\n 0 1 2 3 4 5 6"
        assert self.board.draw_board() == str, "Drawing an empty board works."

    def testMakeMove(self):
        self.board.make_move(Node.Red, 2)
        column = self.board.columns[2]
        assert column.nodes[5].color == Node.Red, "Making a move works."

    def testVerifyMove(self):
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        assert self.board.verify_move(2) is False, "Will alert to an overfilled column"

    def testVerticalWin(self):
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        assert self.board.check_for_victory() == Node.Red, "Vertical win"

    def testNotVerticalWin(self):
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Black, 2)
        assert self.board.check_for_victory() is False, "Vertical is not a win"

    def testHorizontalWin(self):
        self.board.make_move(Node.Red, 1)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 3)
        self.board.make_move(Node.Red, 4)
        assert self.board.check_for_victory() == Node.Red, "Horizontal win"

    def testNotHorizontalWin(self):
        self.board.make_move(Node.Red, 1)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 3)
        self.board.make_move(Node.Black, 4)
        assert self.board.check_for_victory() is False, "Horizontal is not a win"

    def testDiagonalDownWin(self):
        self.board.make_move(Node.Red, 0)
        self.board.make_move(Node.Red, 0)
        self.board.make_move(Node.Red, 0)
        self.board.make_move(Node.Black, 0)
        self.board.make_move(Node.Red, 1)
        self.board.make_move(Node.Red, 1)
        self.board.make_move(Node.Black, 1)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Black, 2)
        self.board.make_move(Node.Black, 3)
        assert self.board.check_for_diagonal_win_downwards() == Node.Black, "Diagonal downwards win"

    def testNotDiagonalDownWin(self):
        self.board.make_move(Node.Black, 1)
        self.board.make_move(Node.Red, 1)
        self.board.make_move(Node.Red, 1)
        self.board.make_move(Node.Black, 1)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Black, 2)
        self.board.make_move(Node.Red, 3)
        self.board.make_move(Node.Black, 3)
        self.board.make_move(Node.Red, 4)
        assert self.board.check_for_victory() is False, "Diagonal downwards not win"

    def testDiagonalUpWin(self):
        self.board.make_move(Node.Black, 6)
        self.board.make_move(Node.Red, 6)
        self.board.make_move(Node.Red, 6)
        self.board.make_move(Node.Red, 6)
        self.board.make_move(Node.Red, 5)
        self.board.make_move(Node.Red, 5)
        self.board.make_move(Node.Red, 5)
        self.board.make_move(Node.Red, 4)
        self.board.make_move(Node.Red, 4)
        self.board.make_move(Node.Red, 3)
        assert self.board.check_for_diagonal_win_upwards() == Node.Red, "Diagonal upwards win"

    def testNotDiagonalUpWin(self):
        self.board.make_move(Node.Black, 6)
        self.board.make_move(Node.Red, 6)
        self.board.make_move(Node.Red, 6)
        self.board.make_move(Node.Red, 6)
        self.board.make_move(Node.Red, 5)
        self.board.make_move(Node.Red, 5)
        self.board.make_move(Node.Red, 5)
        self.board.make_move(Node.Red, 4)
        self.board.make_move(Node.Red, 4)
        self.board.make_move(Node.Black, 3)
        assert self.board.check_for_victory() is False, "Diagonal upwards not win"

class TestAI(unittest.TestCase):
    def setUp(self):
        # Call before every test
        self.board = Board()

    def tearDown(self):
        # Call after every test
        pass # nothing at the moment

    def testVeryEasy(self):
        ai = VeryEasyAI()
        assert type(ai.pick_move(self.board)) is int, "The Very Easy AI picks a move."
        
    def testFindAIWin(self):
        ai = ConnectFourAI()
        self.board.make_move(Node.Black, 0)
        self.board.make_move(Node.Black, 1)
        self.board.make_move(Node.Black, 2)
        assert ai.find_win(self.board, Node.Black) == 3, "The AI claims victory in column 3"

    def testFindAIWinAtZero(self):
        """
        These two tests are to make sure it properly distinguishes between
        zero and False. This was a subtle bug I ran into earlier.
        If the win was available in column zero, == meant it treated it as False.
        This led to it selecting a move at random instead of blocking/winning.
        """
        ai = ConnectFourAI()
        self.board.make_move(Node.Black, 0)
        self.board.make_move(Node.Black, 0)
        self.board.make_move(Node.Black, 0)
        assert ai.find_win(self.board, Node.Black) == 0, "The AI claims victory in column 0"

    def testFindAIWinAtZeroIsNotFalse(self):
        ai = ConnectFourAI()
        self.board.make_move(Node.Black, 0)
        self.board.make_move(Node.Black, 0)
        self.board.make_move(Node.Black, 0)
        assert ai.find_win(self.board, Node.Black) is not False, "The AI claims victory in column 0"

    def testFindHumanWin(self):
        ai = ConnectFourAI()
        self.board.make_move(Node.Red, 3)
        self.board.make_move(Node.Red, 3)
        self.board.make_move(Node.Red, 3)
        assert ai.find_win(self.board, Node.Red) == 3, "The AI sees human victory in column 3"

    def testSeeNoWin(self):
        ai = ConnectFourAI()
        self.board.make_move(Node.Red, 5)
        self.board.make_move(Node.Black, 5)
        assert ai.find_win(self.board, Node.Red) is False, "The AI sees no win for human"

    def testEasyAIBlockWin(self):
        ai = EasyAI()
        self.board.make_move(Node.Red, 3)
        self.board.make_move(Node.Red, 3)
        self.board.make_move(Node.Red, 3)
        assert ai.pick_move(self.board) == 3

    def testAvoidGivingWin(self):
        ai = NormalAI()
        self.board.make_move(Node.Red, 1)
        self.board.make_move(Node.Red, 1)
        self.board.make_move(Node.Black, 2)
        self.board.make_move(Node.Red, 2)
        self.board.make_move(Node.Black, 3)
        self.board.make_move(Node.Red, 3)
        for i in range(0,10):
            move = ai.pick_move(self.board)
            assert move != 4 and move !=0, "The AI will not set up a win."

if __name__ == "__main__":
            unittest.main()
